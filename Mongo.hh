<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder\driver\mongo
{
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin
	};
	use nuclio\plugin\database\
	{
		exception\DatabaseException,
		queryBuilder\CommonQueryBuilderInterface,
		queryBuilder\WheresAndValues,
		datasource\source\Source,
		orm\Model,
		common\DBQuery,
		common\CommonCursorInterface,
		queryBuilder\ExecutionParams
	};
	
	<<provides('queryBuilder::mongo')>>
	class Mongo extends Plugin implements CommonQueryBuilderInterface
	{
		private Source $source;
		
		public static function getInstance(Source $source):Mongo
		{
			$instance=ClassManager::getClassInstance(self::class,$source);
			return ($instance instanceof self)?$instance:new self($source);
		}
		
		public function __construct(Source $source)
		{
			parent::__construct();
			$this->source=$source;
		}
		
		public function execute(ExecutionParams $params):CommonCursorInterface
		{
			$params=new Map((array)$params);
			$target	=$params->get('target');
			$filter	=$params->get('filter');
			$limit	=$params->get('limit');
			$offset	=$params->get('offset');
			$orderBy=$params->get('orderBy');
			$joins	=$params->get('joins');
			
			if (is_null($target))
			{
				throw new DatabaseException('Unable to execute query. Missing target parameter.');
			}
			if (is_null($filter))
			{
				throw new DatabaseException('Unable to execute query. Missing filter parameter.');
			}
			$options=Map{};
			
			if ($limit)
			{
				$options->set('limit',$limit);
			}
			if ($offset)
			{
				$options->set('skip',$offset);
			}
			if ($orderBy)
			{
				$options->set('sort',$orderBy);
			}
			return $this->source->find($target,$filter,$options);
		}
		
		public function parseFilter(string $target, DBQuery $filter):WheresAndValues
		{
			return Pair{Vector{},Vector{}};
		}
	}
}
